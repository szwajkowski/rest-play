package pl.rest.play;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = { "pl.allegro.abtesting.controller" })
public class ControllerConfiguration {

}
