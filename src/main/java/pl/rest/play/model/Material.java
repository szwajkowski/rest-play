package pl.rest.play.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Material {

	private String companyID;

	private String id;

	private String name;

	public String getCompanyID() {
		return companyID;
	}

	@JsonProperty("ID")
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
