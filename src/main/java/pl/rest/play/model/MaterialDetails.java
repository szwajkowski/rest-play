package pl.rest.play.model;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MaterialDetails {

	private String name;

	private String description;

	private String notes;

	private String supplier;

	private String currency;

	private String price;

	private String id;

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getNotes() {
		return notes;
	}

	public String getSupplier() {
		return supplier;
	}

	public String getCurrency() {
		return currency;
	}

	public String getPrice() {
		return price;
	}

	@JsonProperty("ID")
	public String getId() {
		return id;
	}

	public Map<String, Object> getAll() throws IllegalAccessException {
		Map<String, Object> all = new HashMap<String, Object>();
		for (Field field : this.getClass().getDeclaredFields()) {
			all.put(field.getName(), field.get(this));
		}
		return all;
	}
}
