package pl.rest.play.component;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pl.rest.play.model.Company;

@Component
public class CompanyProvider {

	public List<Company> getCompanies() {
		RestTemplate template = new RestTemplate();
		return Arrays.asList(template
				.getForObject("http://193.142.112.220:8337/companyList", Company[].class));
	}
}
