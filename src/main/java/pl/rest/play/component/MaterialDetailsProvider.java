package pl.rest.play.component;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pl.rest.play.model.MaterialDetails;

@Component
public class MaterialDetailsProvider {

	public MaterialDetails getMaterialsDetails(String materialId) {
		RestTemplate template = new RestTemplate();
		return template.getForObject("http://193.142.112.220:8337/materialDetails?ID=" + materialId,
				MaterialDetails.class);
	}
}
