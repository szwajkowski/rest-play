package pl.rest.play.component;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import pl.rest.play.model.Material;

@Component
public class MaterialProvider {

	public List<Material> getMaterialsForCompany(String companyId) {
		RestTemplate template = new RestTemplate();
		return Arrays.asList(template.getForObject("http://193.142.112.220:8337/materialList?companyID="
				+ companyId, Material[].class));
	}

	public List<Material> getMaterials() {
		RestTemplate template = new RestTemplate();
		return Arrays.asList(template.getForObject("http://193.142.112.220:8337/materialList",
				Material[].class));
	}
}
