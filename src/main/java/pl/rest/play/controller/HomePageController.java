package pl.rest.play.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.rest.play.component.CompanyProvider;

@Controller
public class HomePageController {

	@Autowired
	private CompanyProvider provider;

	@RequestMapping("/")
	public String getInformation(Map<String, Object> model) {
		model.put("companies", provider.getCompanies());
		return "info";
	}
}
