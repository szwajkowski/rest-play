package pl.rest.play.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.rest.play.component.MaterialProvider;

@Controller
public class MaterialController {

	@Autowired
	private MaterialProvider provider;

	@RequestMapping("/materials")
	public String getInformation(String companyId, Map<String, Object> model) {
		if (StringUtils.isEmpty(companyId)) {
			model.put("materials", provider.getMaterials());
		} else {
			model.put("materials", provider.getMaterialsForCompany(companyId));
		}
		return "materials";
	}
}
