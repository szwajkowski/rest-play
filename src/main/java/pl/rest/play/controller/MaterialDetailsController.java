package pl.rest.play.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.rest.play.component.MaterialDetailsProvider;

@Controller
public class MaterialDetailsController {

	@Autowired
	private MaterialDetailsProvider provider;

	@RequestMapping("/materialDetails")
	public String getInformation(String materialId, Map<String, Object> model) throws IllegalAccessException {
		model.put("details", provider.getMaterialsDetails(materialId).getAll());
		return "details";
	}
}
