<!DOCTYPE html>

<html lang="en">

<body>
	<h1>Materials</h1>
	
	<ul>
	<#list materials as m>
		<li><a href = "/materialDetails?materialId=${m.id}">${m.name}</a></li>
	</#list>
	</ul>
</body>

</html>