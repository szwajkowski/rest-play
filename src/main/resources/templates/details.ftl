<!DOCTYPE html>

<html lang="en">

<body>
	<h1>Details</h1>
	
	<ul>
	<#list details?keys as k>
		<li>${k} : ${details[k]}</li>
	</#list>
	</ul>
</body>

</html>