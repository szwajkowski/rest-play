<!DOCTYPE html>

<html lang="en">

<body>
	<h1>Companies</h1>
	
	<ul>
	<#list companies as c>
		<li><a href = "/materials?companyId=${c.companyID}" >${c.companyName}</a></li>
	</#list>
	</ul>
</body>

</html>